package main

import (
	"context"
	"fmt"

	api "gitea.com/rammiah/greeting/kitex_gen/api"
)

// EchoImpl implements the last service interface defined in the IDL.
type EchoImpl struct{}

// Greeting implements the EchoImpl interface.
func (s *EchoImpl) Greeting(ctx context.Context, req *api.Request) (resp *api.Response, err error) {
	return &api.Response{
		Message: fmt.Sprintf("hello %s", req.Message),
	}, nil
}
