FROM golang:1.20-alpine as builder
WORKDIR /build
ADD . /build
ENV GOPROXY=https://goproxy.cn,direct
RUN sh /build/build.sh

FROM alpine:latest
WORKDIR /app
COPY --from=builder /build/output /app
EXPOSE 8888
CMD [ "sh", "/app/bootstrap.sh" ]
