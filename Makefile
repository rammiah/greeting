#!/usr/bin/env make

.PHONY: build
build:
	sh build.sh

.PHONY: build-image
build-image:
	docker buildx build -t local/greeting:latest .
