package main

import (
	"log"

	api "gitea.com/rammiah/greeting/kitex_gen/api/echo"
)

func main() {
	svr := api.NewServer(new(EchoImpl))

	err := svr.Run()

	if err != nil {
		log.Println(err.Error())
	}
}
