package main

import (
	"context"
	"fmt"

	"gitea.com/rammiah/greeting/kitex_gen/api"
	"gitea.com/rammiah/greeting/kitex_gen/api/echo"
	"github.com/cloudwego/kitex/client"
)

func main() {
	var (
		cli = echo.MustNewClient("greeting", client.WithHostPorts("192.168.215.2:8888"))
		ctx = context.Background()
		req = &api.Request{
			Message: "world",
		}
	)
	resp, err := cli.Greeting(ctx, req)
	if err != nil {
		panic(err)
	}
	fmt.Printf("resp: %v\n", resp.Message)
}
